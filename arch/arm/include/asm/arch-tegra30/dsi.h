/* SPDX-License-Identifier: GPL-2.0+ */
/*
 *  (C) Copyright 2010
 *  NVIDIA Corporation <www.nvidia.com>
 */

#ifndef __ASM_ARCH_TEGRA30_DSI_H
#define __ASM_ARCH_TEGRA30_DSI_H

#include <asm/arch-tegra/dsi.h>

#endif /* __ASM_ARCH_TEGRA_DSI_H */
